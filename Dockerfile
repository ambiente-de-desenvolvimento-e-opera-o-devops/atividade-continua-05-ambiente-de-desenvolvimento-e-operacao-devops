# Usa a imagem base do Python
#FROM python:3.8
FROM python:3-buster

# Define o diretório de trabalho dentro do container
WORKDIR /app

# Copia o arquivo de requirements para o diretório de trabalho
COPY requirements.txt .

# Instala as dependências do projeto
RUN pip3 install -r requirements.txt

# Copia o código fonte da aplicação para o diretório de trabalho
COPY . .

# Define as variáveis de ambiente necessárias
ENV FLASK_APP=app.py
ENV FLASK_RUN_HOST=0.0.0.0

# Expõe a porta em que a aplicação irá rodar
EXPOSE 5000

# Comando para executar a aplicação
CMD [ "python3", "-m" , "flask", "run"]
