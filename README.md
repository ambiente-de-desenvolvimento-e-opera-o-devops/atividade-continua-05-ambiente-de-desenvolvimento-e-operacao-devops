## Requisitos

- Python 3.x

## Instalação

1. Clone o repositório:

- git clone https://gitlab.com/ambiente-de-desenvolvimento-e-opera-o-devops/atividade-continua-05-ambiente-de-desenvolvimento-e-operacao-devops.git


2. Instale as dependências:

- pip install -r requirements.txt


## Uso

Execute o seguinte comando para iniciar a aplicação:

- python app/main.py

## Testes

Para executar os testes unitários, utilize o seguinte comando:

- python -m unittest discover -s tests -p 'test_*.py'
