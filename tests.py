import unittest

class MyTestCase(unittest.TestCase):
    def test_something(self):
        self.assertEqual(2 + 2, 4)

    def test_another_thing(self):
        self.assertTrue(True)

if __name__ == '__main__':
    unittest.main()
